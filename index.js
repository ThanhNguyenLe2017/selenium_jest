
const { Builder, By, Key, until } = require("selenium-webdriver");


const users = [
    {email: "admin@artistviet.com", password: "secret"},
    {email: "customer@artistviet.com", password: "secret"},
];

let test = async (user) => {
    let driver = await new Builder().forBrowser("chrome").build();
    await driver.get("http://artistviet.vicoders.com");

    await driver.findElement(By.css("#signin")).click();

    await driver.wait(until.elementLocated(By.css("#email")), 12000);
    await driver.findElement(By.css("#email")).sendKeys(user.email);
    await driver.findElement(By.css("#password")).sendKeys(user.password);
};

users.forEach(item => test(item));